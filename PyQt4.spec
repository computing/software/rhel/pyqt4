%if 0%{?fedora} || 0%{?rhel} >= 7
%global with_python3 1
%global qtassistant 1
%global webkit 1
%endif

%{!?python3_inc:%global python3_inc %(%{__python3} -c "from distutils.sysconfig import get_python_inc; print(get_python_inc(1))")}
%global python3_dbus_dir %(%{__python3} -c "import dbus.mainloop; print(dbus.mainloop.__path__[0])")
%{!?python3_other_inc:%global python3_other_inc %(%{__python3_other} -c "from distutils.sysconfig import get_python_inc; print(get_python_inc(1))")}
%global python3_other_dbus_dir %(%{__python3_other} -c "import dbus.mainloop; print(dbus.mainloop.__path__[0])")


Summary: Python bindings for Qt4
Name: 	 PyQt4
Version: 4.12.1
Release: 7.3%{?dist}

# GPLv2 exceptions(see GPL_EXCEPTIONS*.txt)
License: (GPLv3 or GPLv2 with exceptions) and BSD
Url:     http://www.riverbankcomputing.com/software/pyqt/
%if 0%{?snap:1}
Source0:  http://www.riverbankcomputing.com/static/Downloads/PyQt4/PyQt-x11-gpl-%{version}%{?snap:-snapshot-%{snap}}.tar.gz
%else
Source0:  http://sourceforge.net/projects/pyqt/files/PyQt4/PyQt-%{version}/PyQt4_gpl_x11-%{version}.tar.gz
%endif

Source2: pyuic4.sh

## upstreamable patches

## upstream patches
# fix FTBFS on ARM
Patch60:  qreal_float_support.diff

# rhel patches
Patch300: PyQt-x11-gpl-4.11-webkit.patch

BuildRequires: chrpath
BuildRequires: dbus-python
BuildRequires: findutils
BuildRequires: gcc-c++
BuildRequires: pkgconfig(dbus-1) pkgconfig(dbus-python)
BuildRequires: pkgconfig(phonon)
BuildRequires: pkgconfig(QtDBus) pkgconfig(QtDeclarative) pkgconfig(QtDesigner)
BuildRequires: pkgconfig(QtGui) pkgconfig(QtHelp) pkgconfig(QtMultimedia)
BuildRequires: pkgconfig(QtNetwork) pkgconfig(QtOpenGL)
BuildRequires: pkgconfig(QtScript) pkgconfig(QtScriptTools)
BuildRequires: pkgconfig(QtSql) pkgconfig(QtSvg) pkgconfig(QtTest)
BuildRequires: pkgconfig(QtXml) pkgconfig(QtXmlPatterns)
BuildRequires: pkgconfig(QtAssistantClient) pkgconfig(QtWebKit)
#BuildRequires: sip-devel >= 4.19

BuildRequires: python%{python3_pkgversion}-dbus
BuildRequires: python%{python3_pkgversion}-devel 
BuildRequires: python%{python3_pkgversion}-sip-devel >= 4.19

Requires: dbus-python
%{?_qt4_version:Requires: qt4%{?_isa} >= %{_qt4_version}}
%{?_sip_api:Requires: sip-api(%{_sip_api_major}) = %{_sip_api}}

%if 0%{?webkit}
# when -webkit was split out
Obsoletes: PyQt4 < 4.11.4-8
%endif

Provides: python-qt4 = %{version}-%{release}
Provides: python2-qt4 = %{version}-%{release}
Provides: python2-PyQt4 = %{version}-%{release}
Provides: pyqt4 = %{version}-%{release}

%global __provides_exclude_from ^(%{python2_sitearch}/.*\\.so|%{python3_sitearch}/.*\\.so|%{_qt4_plugindir}/.*\\.so)$

%description
These are Python bindings for Qt4.

%package doc
Summary: PyQt4 developer documentation and examples
BuildArch: noarch
# when split happened, upgrade path
Obsoletes: PyQt4-devel < 4.10.3-6
Obsoletes: python3-PyQt4-devel < 4.10.3-6
Provides: python-qt4-doc = %{version}-%{release}
Provides: python2-qt4-doc = %{version}-%{release}
Provides: python2-PyQt4-doc = %{version}-%{release}
%description doc
%{summary}.

%if 0%{?qtassistant}
%package -n python%{python3_pkgversion}-%{name}-assistant
Summary: Python 3 bindings for QtAssistant
Provides: python%{python3_pkgversion}-qt4-assistant = %{version}-%{release}
Requires: python%{python3_pkgversion}-%{name}%{?_isa} = %{version}-%{release}
%description -n python%{python3_pkgversion}-%{name}-assistant
%{summary}.
%endif

%if 0%{?webkit}
%package -n python%{python3_pkgversion}-%{name}-webkit
Summary: Python3 bindings for Qt4 Webkit
Obsoletes: python%{python3_pkgversion}-PyQt4 < 4.11.4-8
Provides: python%{python3_pkgversion}-qt4-webkit = %{version}-%{release}
Requires:  python%{python3_pkgversion}-PyQt4%{?_isa} = %{version}-%{release}
%description -n python%{python3_pkgversion}-%{name}-webkit
%{summary}.
%endif


# The bindings are imported as "PyQt4", hence it's reasonable to name the
# Python 3 subpackage "python3-PyQt4", despite the apparent tautology
%package -n python%{python3_pkgversion}-%{name}
Summary: Python 3 bindings for Qt4
Requires: python%{python3_pkgversion}-dbus
%{?_qt4_version:Requires: qt4%{?_isa} >= %{_qt4_version}}
#%{?_sip_api:Requires: python3-sip-api(%{_sip_api_major}) = %{_sip_api}}
Requires: python36-sip >= 4.19.8-2
%if 0%{?webkit}
Obsoletes: python%{python3_pkgversion}-PyQt4 < 4.11.4-8
%endif
Provides: python%{python3_pkgversion}-qt4 = %{version}-%{release}
%description -n python%{python3_pkgversion}-%{name}
These are Python 3 bindings for Qt4.

%package -n python%{python3_pkgversion}-%{name}-devel
Summary: Python 3 bindings for Qt4
%if 0%{?webkit}
Provides: python%{python3_pkgversion}-%{name}-webkit-devel = %{version}-%{release}
Requires: python%{python3_pkgversion}-%{name}-webkit%{?_isa} = %{version}-%{release}
%endif
Provides: python%{python3_pkgversion}-qt4-devel = %{version}-%{release}
Requires: python%{python3_pkgversion}-%{name}%{?_isa} = %{version}-%{release}
Requires: python%{python3_pkgversion}-sip-devel
# when split happened, upgrade path
Obsoletes: python%{python3_pkgversion}-PyQt4-devel < 4.10.3-6
%description -n python%{python3_pkgversion}-%{name}-devel
Files needed to build other Python 3 bindings for C++ classes that inherit
from any of the Qt4 classes (e.g. KDE or your own).


%prep
%setup -q -n PyQt4_gpl_x11-%{version}%{?snap:-snapshot-%{snap}}

# save orig for comparison later
cp -a ./sip/QtGui/opengl_types.sip ./sip/QtGui/opengl_types.sip.orig
%patch60 -p1 -b .arm
%if ! 0%{?webkit}
%patch300 -p1 -b .webkit
%endif

# permissions, mark examples non-executable
find examples/ -name "*.py" | xargs chmod a-x


%build

QT4DIR=%{_qt4_prefix}
PATH=%{_qt4_bindir}:$PATH ; export PATH

# Python 3 build:
mkdir %{_target_platform}-python3
pushd %{_target_platform}-python3
%{__python3} ../configure.py \
  --assume-shared \
  --confirm-license \
  --no-timestamp \
  --qmake=%{_qt4_qmake} \
  --no-qsci-api \
  --sipdir=%{_datadir}/python3-sip/PyQt4 \
  --verbose \
  CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" LFLAGS="%{?__global_ldflags}"

make %{?_smp_mflags}
popd


%install

# Install Python 3 first, and move aside any executables, to avoid clobbering
# the Python 2 installation:
make install DESTDIR=%{buildroot} INSTALL_ROOT=%{buildroot} -C %{_target_platform}-python3

# likewise, remove Python 2 code from the Python 3.1 directory:
rm -rfv %{buildroot}%{python3_sitearch}/PyQt4/uic/port_v2/
rm -rfv %{buildroot}%{python3_other_sitearch}/PyQt4/uic/port_v2/

# install pyuic4 wrapper to support both python2/python3
rm -fv %{buildroot}%{_bindir}/pyuic4
install -p -m755 -D %{SOURCE2} \
  %{buildroot}%{_bindir}/pyuic4
sed -i \
  -e "s|@PYTHON3@|%{__python3}|g" \
  -e "s|@PYTHON2@|%{__python2}|g" \
  %{buildroot}%{_bindir}/pyuic4


%check
# verify opengl_types.sip sanity
diff -u ./sip/QtGui/opengl_types.sip.orig \
        ./sip/QtGui/opengl_types.sip ||:


%files doc
%doc doc/*
%doc examples/

%files -n python%{python3_pkgversion}-%{name}
%doc NEWS README
%doc LICENSE
%{python3_dbus_dir}/qt.so
%dir %{python3_sitearch}/PyQt4/
%{python3_sitearch}/PyQt4/__init__.py*
%{python3_sitearch}/PyQt4/__pycache__/
%{python3_sitearch}/PyQt4/pyqtconfig.py*
%{python3_sitearch}/PyQt4/phonon.so
%{python3_sitearch}/PyQt4/Qt.so
%{python3_sitearch}/PyQt4/QtCore.so
%{python3_sitearch}/PyQt4/QtDBus.so
%{python3_sitearch}/PyQt4/QtDeclarative.so
%{python3_sitearch}/PyQt4/QtDesigner.so
%{python3_sitearch}/PyQt4/QtGui.so
%{python3_sitearch}/PyQt4/QtHelp.so
%{python3_sitearch}/PyQt4/QtMultimedia.so
%{python3_sitearch}/PyQt4/QtNetwork.so
%{python3_sitearch}/PyQt4/QtOpenGL.so
%{python3_sitearch}/PyQt4/QtScript.so
%{python3_sitearch}/PyQt4/QtScriptTools.so
%{python3_sitearch}/PyQt4/QtSql.so
%{python3_sitearch}/PyQt4/QtSvg.so
%{python3_sitearch}/PyQt4/QtTest.so
%{python3_sitearch}/PyQt4/QtXml.so
%{python3_sitearch}/PyQt4/QtXmlPatterns.so
%{python3_sitearch}/PyQt4/uic/
%{_qt4_plugindir}/designer/*

%if 0%{?qtassistant}
%files -n python%{python3_pkgversion}-%{name}-assistant
%{python3_sitearch}/PyQt4/QtAssistant.so
%endif

%if 0%{?webkit}
%files -n python%{python3_pkgversion}-%{name}-webkit
%{python3_sitearch}/PyQt4/QtWebKit.so
%endif

%files -n python%{python3_pkgversion}-%{name}-devel
%{_bindir}/pylupdate4
%{_bindir}/pyrcc4
%{_bindir}/pyuic4
%{_datadir}/python3-sip/PyQt4/



%changelog
* Thu Oct 10 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 4.12.1-7.3
- Fix -devel subpackage conflict with earlier versions of PyQt4

* Mon Sep 16 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 4.12.1-7.2
- Drop python2, python3.4 support

* Fri Nov 16 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 4.12.1-7.1
- Add python3.6 support

* Tue Feb 20 2018 Rex Dieter <rdieter@fedoraproject.org> - 4.12.1-7
- BR: gcc-c++

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Fri Oct 27 2017 Merlin Mathesius <mmathesi@redhat.com> - 4.12.1-5
- Cleanup spec file conditionals

* Mon Jul 31 2017 Than Ngo <than@redhat.com> - 4.12.1-4
- fixed bz#1348514 - Arbitrary code execution due to insecure loading
  of Python module from CWD

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jul 05 2017 Rex Dieter <rdieter@fedoraproject.org> - 4.12.1-2
- rebuild (sip)

* Sun Jul 02 2017 Rex Dieter <rdieter@fedoraproject.org> - 4.12.1-1
- PyQt4-4.12.1

* Thu Mar 30 2017 Rex Dieter <rdieter@fedoraproject.org> - 4.12-4
- rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.12-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Jan 27 2017 Rex Dieter <rdieter@fedoraproject.org> - 4.12-2
- update provides filtering

* Sun Jan 01 2017 Rex Dieter <rdieter@math.unl.edu> - 4.12-1
- PyQt4-4.12

* Fri Dec 09 2016 Charalampos Stratakis <cstratak@redhat.com> - 4.11.4-16
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.11.4-15
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Wed Apr 20 2016 Rex Dieter <rdieter@fedoraproject.org> - 4.11.4-14
- rebuild (qt)

* Mon Apr 18 2016 Rex Dieter <rdieter@fedoraproject.org> - 4.11.4-13
- Provides: python2-qt4/python2-PyQt4 (#1249422)

* Mon Apr 18 2016 Rex Dieter <rdieter@fedoraproject.org> - 4.11.4-12
- rebuild (qt)

* Wed Apr 13 2016 Rex Dieter <rdieter@fedoraproject.org> - 4.11.4-11
- rebuid (sip)

* Wed Mar 02 2016 Rex Dieter <rdieter@fedoraproject.org> 4.11.4-10
- -webkit: add Provides: to match those of main pkg

* Wed Mar 02 2016 Rex Dieter <rdieter@fedoraproject.org> 4.11.4-9
- rebase -webkit.patch, use safer subdir builds

* Mon Feb 29 2016 Rex Dieter <rdieter@fedoraproject.org> - 4.11.4-8
- don't remove anything from uic/widget-plugins (see also #1294307)
- -webkit subpkg

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 4.11.4-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Jan 31 2016 Rex Dieter <rdieter@fedoraproject.org> 4.11.4-6
- explicitly set CFLAGS,CXXFLAGS,LFLAGS

* Thu Nov 12 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.11.4-5
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Tue Nov 10 2015 Than Ngo <than@redhat.com> - 4.11.4-4
- rebuild

* Tue Oct 13 2015 Robert Kuska <rkuska@redhat.com> - 4.11.4-3
- Rebuilt for Python3.5 rebuild

* Tue Jun 16 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.11.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri Jun 12 2015 Rex Dieter <rdieter@fedoraproject.org> 4.11.4-1
- PyQt4-4.11.4

* Fri Jun 05 2015 Rex Dieter <rdieter@fedoraproject.org> - 4.11.3-5
- drop qscintilla conditional
- -python3-devel: include binaries, use pyuic4 wrapper (see also #1193107)

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 4.11.3-4
- Rebuilt for GCC 5 C++11 ABI change

* Wed Mar 25 2015 Rex Dieter <rdieter@fedoraproject.org> 4.11.3-3
- rebuild (sip)

* Wed Feb 25 2015 Rex Dieter <rdieter@fedoraproject.org> 4.11.3-2
- rebuild (sip)

* Mon Nov 10 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11.3-1
- PyQt4-4.11.3

* Thu Nov 06 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11.2-2
- python2_sitelib should be python2_sitearch (#1161121)

* Mon Sep 15 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11.2-1
- PyQt4-4.11.2

* Fri Aug 15 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.11.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Aug 13 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11.1-2
- rebuild (qt/phonon)

* Sun Jul 06 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11.1-1
- PyQt4-4.11.1

* Fri Jun 06 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.11.1-0.3.9d5a6843b580
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Jun 04 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11.1-0.2.9d5a6843b580
- rebuild for new qscintilla (#1104559)

* Sun Jun 01 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.11.1-0.1.9d5a6843b580
- PyQt4-4.11.1 snapshot (fix FTBFS)
- re-enable -assistant subpkg

* Wed May 28 2014 Rex Dieter <rdieter@fedoraproject.org> 4.11-1
- PyQt-4.11
- Obsoletes: PyQt4-assistant
- use configure-ng.py (may as well, configure.py is broken)

* Mon May 12 2014 Rex Dieter <rdieter@fedoraproject.org> 4.10.4-2
- rebuild (f21-python)

* Sun Mar 16 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.10.4-1
- PyQt-4.10.4 (#1076001)
- s/python/python2/

* Fri Mar 14 2014 Rex Dieter <rdieter@fedoraproject.org> 4.10.3-6
- polish/improve uic multilib issues (#1076346)
- -doc.noarch,-qsci-api subpkgs
- python3-PyQt4: python3-dbus support

* Mon Feb 17 2014 Rex Dieter <rdieter@fedoraproject.org> 4.10.3-5
- flesh out python(3)-qt4 related provides

* Fri Dec 06 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.3-4
- rebuild (phonon)

* Thu Nov 21 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.3-3
- simpler phonon_detect.patch

* Thu Nov 14 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.3-2
- fix build against phonon-4.7+ (kde#306261)

* Wed Oct 16 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.3-1
- 4.10.3

* Mon Oct 07 2013 Than Ngo <than@redhat.com> - 4.10.2-3
- fix license tag
- add missing buildroot

* Fri Aug 02 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Mon Jun 17 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.2-1
- 4.10.2

* Tue May 07 2013 Than Ngo <than@redhat.com> - 4.10.1-5
- add qtassistant macro

* Fri May 03 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.1-4
- fix dbus/mainloop hacks (#957867)

* Thu May 02 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.1-3
- ImportError: cannot import name uic (#958736)

* Fri Apr 26 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.1-2
- filter private shared objects
- %%{python_sitelib}/dbus/mainloop/qt.so should be in %%python_sitearch (#957260)
- .spec cleanup
- -assistant subpkg

* Mon Apr 22 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10.1-1
- 4.10.1

* Tue Apr 02 2013 Than Ngo <than@redhat.com> - 4.10-3
- adapt rhel patch

* Fri Mar 22 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10-2
- introduce qscintilla, webkit feature macros

* Sun Mar 03 2013 Rex Dieter <rdieter@fedoraproject.org> 4.10-1
- 4.10

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.9.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jan 04 2013 Than Ngo <than@redhat.com> - 4.9.6-2
- adapt rhel patch

* Sun Dec 09 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.6-1
- 4.9.6

* Sun Oct 28 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.5-3
- rebuild (sip)

* Thu Oct 11 2012 Than Ngo <than@redhat.com> - 4.9.5-2
- update webkit patch

* Mon Oct 01 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.5-1
- PyQt-4.9.5

* Sat Aug 04 2012 David Malcolm <dmalcolm@redhat.com> - 4.9.4-5
- rebuild for https://fedoraproject.org/wiki/Features/Python_3.3

* Fri Aug  3 2012 David Malcolm <dmalcolm@redhat.com> - 4.9.4-4
- make with_python3 be conditional on fedora

* Mon Jul 30 2012 Than Ngo <than@redhat.com> - 4.9.4-3
- update webkit patch

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.9.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Jun 28 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.4-1
- 4.9.4

* Sun Jun 24 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.3-1
- 4.9.3

* Fri Jun 22 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.2-1
- 4.9.2

* Thu Jun 21 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.1-4
- PyQt4 opengl-types.sip multilib conflict (#509415)

* Fri May 04 2012 Than Ngo <than@redhat.com> - 4.9.1-3
- add rhel/fedora condition

* Sun Mar  4 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 4.9.1-2
- Add upstream patch (via Debian) to fix FTBFS on ARM

* Sat Feb 11 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.1-1
- 4.9.1

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.9-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Jan 09 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9-2
- upstream doItemsLayout patch

* Fri Dec 23 2011 Rex Dieter <rdieter@fedoraproject.org> 4.9-1
- 4.9

* Tue Dec 20 2011 Than Ngo <than@redhat.com> - 4.8.6-4
- Provides: pyqt4

* Wed Dec 14 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.6-3
- -devel: Provides: -webkit-devel

* Fri Nov 18 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.6-2
- Provides: python(3)-qt4

* Wed Oct 26 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.6-1
- 4.8.6

* Mon Oct 17 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.5-2
- pkgconfig-style deps
- Provides: -webkit
- s/python3-PyQt4/python3-%%name/

* Wed Aug 10 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.5-1
- 4.8.5

* Sat Jul 23 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.4-4
- rebuild (qt48)

* Thu Jun 16 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.4-3
- rebuild

* Wed Jun 08 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.4-2
- squash more rpaths

* Mon May 02 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.4-1
- 4.8.4

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Jan 24 2011 Rex Dieter <rdieter@fedoraproject.org> 4.8.3-1
- PyQt4-x11-gpl-4.8.3

* Sat Jan 15 2011 Rex Dieter <rdieter@fedoraproject.org> - 4.8.3-0.1.454d07a16153
- 4.8.3 snapshot
- Little typo (#668289)

* Fri Dec 24 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.8.2-1
- PyQt4-x11-gpl-4.8.2

* Sat Oct 30 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.8.1-1
- PyQt4-x11-gpl-4.8.1

* Wed Oct 27 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.8-3
- fix pyuic_shbang.patch
- drop implicit-linking patch (no longer needed)

* Sun Oct 24 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.8-2
- drop BR: qt-assistant-adp-devel (these deprecated bindings are no longer included)

* Sat Oct 23 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.8-1
- PyQt-x11-gpl-4.8

* Sat Oct 02 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.7-3
- backport patch to fix kdebindings/pykde ftbfs
- drop sip-devel min version a bit to match reality

* Wed Sep 29 2010 jkeating - 4.7.7-2
- Rebuilt for gcc bug 634757

* Wed Sep 22 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.7-1
- PyQt-x11-gpl-4.7.7

* Mon Sep 13 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.6-2
- backport pyuic fix for python2

* Thu Sep 09 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.6-1
- PyQt-x11-gpl-4.7.6

* Wed Aug 25 2010 Thomas Spura <tomspur@fedoraproject.org> - 4.7.4-3
- rebuild with python3.2
  http://lists.fedoraproject.org/pipermail/devel/2010-August/141368.html

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 4.7.4-2
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Wed Jul 14 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.4-1
- PyQt-x11-gpl-4.7.4

* Sat May 08 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.3-3
- BR: qt4-webkit-devel

* Mon Apr 26 2010 David Malcolm <dmalcolm@redhat.com> - 4.7.3-2
- add python 3 subpackages (#586196)

* Sat Apr 17 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.3-1
- PyQt-x11-gpl-4.7.3

* Sun Mar 21 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 4.7.2-2
- rebuild against fixed qt to get QtMultimedia detected properly

* Thu Mar 18 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7.2-1
- PyQt-x11-gpl-4.7.2

* Sun Mar 14 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 4.7-5
- fix implicit linking when checking for QtHelp and QtAssistant
- remove Python 3 code from Python 2.6 directory, fixes FTBFS (#564633)

* Sat Mar 13 2010 Kevin Kofler <Kevin@tigcc.ticalc.org> - 4.7-4
- BR qt-assistant-adp-devel

* Tue Feb 23 2010 Than Ngo <than@redhat.com> - 4.7-3
- fix multilib conflict because of timestamp

* Sun Feb 14 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7-2
- rebuild

* Fri Jan 15 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7-1
- PyQt-x11-gpl-4.7 (final)

* Thu Jan 07 2010 Rex Dieter <rdieter@fedoraproject.org> - 4.7-0.1.20091231
- PyQt-x11-gpl-4.7-snapshot-20091231

* Fri Nov 27 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.2-5
- phonon bindings missing (#541685)

* Wed Nov 25 2009 Than Ngo <than@redhat.com> - 4.6.2-4
- fix conditional for RHEL

* Wed Nov 25 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.2-3
- PyQt4-4.6.2 breaks QStringList in QVariant, rebuild with sip-4.9.3 (#541211)

* Wed Nov 25 2009 Than Ngo <than@redhat.com> - 4.6.2-2
- fix conditional for RHEL

* Fri Nov 20 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.2-1
- PyQt4-4.6.2

* Thu Nov 19 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.1-2.1
- rebuild (for qt-4.6.0-rc1, f13+)

* Mon Nov 16 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.1-2
- Requires: sip-api(%%_sip_api_major) >= %%_sip_api

* Fri Oct 23 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.1-1
- PyQt4-4.6.1

* Thu Oct 15 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.6.1-0.1.20091014
- PyQt4-4.6.1-snapshot-20091014 (#529192)

* Tue Jul 28 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5.4-1
- PyQt4-4.5.4

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jul 16 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5.2-1
- PyQt4-4.5.2

* Thu Jul 02 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5.1-2
- fix build with qt-4.5.2
- PyQt4-devel multilib conflict (#509415)

* Tue Jun 16 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5.1-1
- PyQt-4.5.1

* Fri Jun 05 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5-1
- PyQt-4.5

* Thu May 21 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5-0.2.20090520
- fix generation of sip_ver

* Thu May 21 2009 Rex Dieter <rdieter@fedoraproject.org> - 4.5-0.1.20090520
- PyQt-4.5-snapshot-20090520
